class Article{

    constructor(titre,soustitre,auteur,texte){
        this.titre = titre;
        this.soustitre = soustitre;
        this.auteur = auteur;
        this.texte = texte
    }

} 
const articlesBlog = [
    new Article( "comprendre le PHP", "Veille Technologique", "Revolio Clockberg jr", "Le texte commence ici. lorem ipsum...." ),
    new Article( "comprendre le Javascript", "Veille Technologique", "Damien Robitaille", "Le texte commence ici. lorem ipsum...." ),
    new Article( "Difference entre Java et Javascript", "Veille Technologique", "Arthur Couture", "Le texte commence ici. lorem ipsum...." ),
    new Article( "Node.js ou Deno.js", "Veille Technologique", "Justin Trudeau", "Le texte commence ici. lorem ipsum...." ),
    new Article( "comprendre le PHP", "Veille Technologique", "Dave Morissette", "Le texte commence ici. lorem ipsum...." ),
    new Article( "comprendre le PHP", "Veille Technologique", "Packualito Camel", "Le texte commence ici. lorem ipsum...." ),
    new Article( "comprendre le PHP", "Veille Technologique", "Mia Wallace", "Le texte commence ici. lorem ipsum...." ),
    new Article( "comprendre le PHP", "Veille Technologique", "Nadir Maguemoun", "Le texte commence ici. lorem ipsum...." ),
    new Article( "comprendre le PHP", "Veille Technologique", "Jane Fonda", "Le texte commence ici. lorem ipsum...." ),
    new Article( "comprendre le PHP", "Veille Technologique", "X AE-12", "Le texte commence ici. lorem ipsum...." ),
]

exports.getArticlesByTitre = function(titre){
    return articlesBlog[titre]; 
 }

exports.allArticlesBlog = articlesBlog;

