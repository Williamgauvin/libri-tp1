class Formation{

    constructor(type,nom,description,cout){
        this.type = type;
        this.nom = nom;
        this.description = description;
        this.cout = cout;
    }

} 
const formations = [
    
    new Formation("MongoDB", "Introduction", "Apprenez les bases de MongoDB", "39.99$" ),
    new Formation("MongoDB", "Principes", "Faites vos premiers pas", "39.99$" ),
    new Formation("MongoDB", "Intermediaire", "Application de MongoDB", "39.99$" ),
    new Formation("MongoDB", "Avance", "Niveau supperieur MongoDB", "39.99$" ),
    new Formation("MongoDB", "Certification", "Obtenez une certification", "99.99$" ),
    new Formation("Nodejs", "Introduction", "Apprenez les bases de Nodejs", "39.99$" ),
    new Formation("Nodejs", "Principes", "Faites vos premiers pas", "39.99$" ),
    new Formation("Nodejs", "Intermediaire", "Application de Nodejs", "39.99$" ),
    new Formation("Nodejs", "Avance", "Niveau supperieur Nodejs", "39.99$" ),
    new Formation("Nodejs", "Certification", "Obtenez une certification", "99.99$" ),
];

exports.getByType = function(type){
    var formations = [];
    for ( let i = 0; i < formations.length ; i++){
        if(formations[i].type == type){
            formations.push(type);
        }
    }
    return formations;
}



exports.allFormation = formations;


