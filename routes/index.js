var express = require('express');
var router = express.Router();
let formations = require("../modules/formations.js");
let articlesBlog = require("../modules/articles.js");
// let contacts = require("../modules/contact.js");



// /* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Libri' });
})

/* Get formations page */
router.get('/formations', function(req, res, next) {
  res.render('formations', { title: 'Formations', formations: formations.allFormation });
})


    /* Get Formation>MongoDB page */
    router.get('/formations/mongodb',function(req, res, next) {
      res.render('formation', { formations:formations.getByType(req.params.type) });
})


/* Get Blog page */
  router.get('/blog', function(req, res, next) {
    res.render('blog', { title: 'Blog', articlesBlog: articlesBlog.allArticlesBlog });
})


//     /* Get Blog > creer Article page */
  
  // JE ne comprend pas pourquoi ca marche pas. Pas capable d'afficher le contenu du tableau {{article.titre etc.}}
  router.get('/blog/:titre',function(req,res,next){
   res.render('article',{ article:articlesBlog.getArticlesByTitre(req.params.titre)})
  })

// /* GET contact page. */
//  router.get('/contact', function(req, res, next) {
//    res.render('contact', { title: 'Contactez-nous!' });
//  })

//  router.post('/contact/:name', function(req, res, next) {
//    res.render('merci');
//  })

//  routeur.post('/contact/:name',  function(req, res, next) {
//    let contactTemp = Contacts.addContact(req.body)
//    if(contactTemp){
//      res.render('contacts',{ contacts: contacts.allContacts})
//    }
//  })

module.exports = router;
